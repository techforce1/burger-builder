FROM node:10-alpine
WORKDIR /burgerbuilder
COPY . /burgerbuilder/
RUN npm install
CMD [ "npm", "start" ]
