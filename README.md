# Burger Builder

This is the code for the knowledge session of February 13, 2019. The
workshop is meant for the participants to get familiar with ReactJS.

## Requirements

- NodeJS and [NPM](https://nodejs.org/en/download/)
  or
- [Vagrant](https://www.vagrantup.com/docs/installation/)

In case you're planning to run the application without vagrant and
from Nodejs directly, I would suggest to install NodeJS and NPM using
[NVM](https://github.com/creationix/nvm)

## Installation

The application can be installed in two seperate ways: using Vagrant
or natively on your computer.

### The Vagrant way

When using Vagrant to install the application, navigate to the
`Vagrant` file in the command prompt. In this case, the `Vagrant`
file is conveniently located in the `project_root` folder.
After you've navigated to the `Vagrant` file, just execute the
command:

```
$ vagrant up
```

### The Native way

Without Vagrant, you need to install the dependencies yourself. You
can install the dependencies with the following command:

```
$ npm install
```

Now you're ready to go bananas!

## Deployment

- [Firebase](https://firebase.google.com/docs/cli/)

This is an application hosted on [Google Firebase](https://firebase.google.com/)
If you want to deploy the application, make sure you install the
Firebase CLI first and login using the following command:

```
$ firebase login
```

After the login process is complete, you can use the following
commands:

- `firebase deploy`: deploy the entire application to firebase
- `firebase deploy --only hosting`: Only deploys the static files in
  the `{project_folder}/build` folder.
- `firebase serve`: simulates a firebase environment locally. Note
  that this is not necessary to run the application, but only for
  testing purposes.

Be aware that before deploying, the site has to be build first (this
is **NOT** done automatically, not even when running the application).
To build the application, you should run:

```
$ npm run build
```

## Running the App

To start and run the application you simply have to execute the
command: `$ npm start` from within the root folder of the application.
When running the application with Vagrant, first ssh into the
container and navigate to the root folder with the following commands:

```
$ vagrant ssh
$ cd /vagrant
```

## Assignments

For the workshop, a list of assignments are made available for you to
expand on the current application. The application only contains the
bare minimum to be able to run, together with an CSS framework (
[Material UI](https://material-ui.com/)) and a first version of the
API service layer. Before starting with the assignments, it's a good
idea to get yourself familiar with the project a little bit and take
a look around the folder structure.

The assignments vary in difficulty (they get harder further down) and
are mostly independent, stand-alone features (unless stated
otherwise). If you feel convident you understand the basics of React,
I would suggest you take a look at the assignments and start with one
that you feel comfortable with.

To make the assignments a little more easy, please familiarize
yourself with the Material UI library a bit. You don't have to know
exactly how to implement everything strait away, but it would help if
you know what kind of components are made available to you.

### More ingredients

Add a few more ingredients to your builder for people with different
taste.
A few suggestions on what to add: cheese, bacon, tomatoes and secret
sauce!

### Multiple ingredients

Right now, only one ingredient can be chosen to be put on the
hamburger. For an even more tastier hamburger, allow the users to add
multiple ingredients to their hamburger.
A few things to consider:

- Users are dumb and they make mistakes. Allow the users to cancel an
  ingredient
- Do you want the user to choose their own order of ingredients or
  does your franchise dictate the order?
- Do you want the user to choose an unlimited amount of ingredients (
  I'll have a bun with 24 patties please), or do you limit the amount?

### Add price

Give the burger a base price together with a price for each
ingredient, preferably including VAT (no shady businesses here). Make
sure to show this price to the user as well

### Authentication

Enable (and maybe beautify) the Sign-In and Sign-Up page of the
application (these are located in the `src/views/Auth` folder). This
Can be done as a separarate page, or in a dialog window (take a look
at the Material UI library).
In case you're feelin' frisky; add some route guarding to the whole
application ;). However, in order to do so I would suggest to finish
the [_Higher order components_](#higher-order-components) assignment
first.

### Place order

Allow users to create and save orders in the Firebase database. To
see how you can connect to the Firebase database, please check the
`API.ts` file in the `src/http` folder.
In case you enabled [_Authentication_](#authentication) earlier, try
to think of a way to incorperate the user in the saved order's model.

### Review order

In order to complete this assignment, you need to finish the
[_Authentication_](#authentication) and [_Place order_](#place-order)
assignment first.
Get all the orders that are placed by the currently logged in user
and display their previously placed orders. Maybe they can even base
their new order off a previous order.

### Orders dashboard

In order to complete this assignment, you need to finish the
[_Authentication_](#authentication) and [_Place order_](#place-order)
assignment first.
Create some sort of dashboard the your franchise where you can see
all the incomming orders. Your haute-cuisine chefs can use this
dashboard to ultimately prepare the hamburgers.

### Streaming data

In order to complete this assignment, you need to finish the
[_Orders dashboard_](#orders-dashboard) assignment first.
The Firebase API allows for streaming data (included in the `API.ts`
file). Use this to listen for incoming orders and show them on your
dashboard in real-time. Right now, only the `value` endpoint of the
Firebase API is supported in the `API.ts` file. However, there are
more endpoints that you can listen to if you want to:

- `child_added`
- `child_changed`
- `child_removed`
- `child_moved`
  For more information about this, please check the [Firebase API
  documentation](https://firebase.google.com/docs/database/web/lists-of-data#listen_for_child_events)

## Future assignment ideas

Over here is a section of ideas that could be implented later on.

### Higher order components

In order to complete this assignment, you need to at least finish the
[_Authentication_](#authentication) or [_Place order_](#place-order)
assignment first.
Higher order components (HOC) are basically wrappers that you can put
around components to inject data and/or functionalities. An example
of a HOC is the `withStyles` function from the `@material-ui/core`
library that is used in this application. It wraps the component with
the JSS (CSS in JS) styles and makes them available in the component.

In this application, HOCs can be provided to make Route Guards and
handle loading orders from the database.

For more information on Higher order components, please check this
[link](https://reactjs.org/docs/higher-order-components.html)

### Store management

In order to complete this assignment, you need to at least finish the
[_Orders dashboard_](#orders-dashboard) assignment first.
Store management is a way of caching back-end data in the application.
In order to do so, multiple packages are available like `Redux` and
`MobX`. These packages help you cache the data and disperce them
throughout your application.

### Offline available

In order to complete this assignment, you need to at least finish the
[_Orders dashboard_](#orders-dashboard) assignment first.
Right now, the API can only send requests when the internet is on. In
order to make the application more robust, enable the offline
capabilities from the Firebase API. For more information, please
check this [link](https://firebase.google.com/docs/database/web/offline-capabilities)
