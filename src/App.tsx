import * as React from 'react'
import { Router } from 'react-router-dom'
import createHistory from 'history/createBrowserHistory'

import Routes from './Routes'
import Navigation from './views/Navigation'

const browserHistory = createHistory()

class App extends React.Component {
  public render() {
    return (
      <Router history={browserHistory}>
        <Navigation>
          <Routes />
        </Navigation>
      </Router>
    )
  }
}

export default App
