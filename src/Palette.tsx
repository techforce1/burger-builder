import React from 'react'
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles'
import blue from '@material-ui/core/colors/blue'
import red from '@material-ui/core/colors/red'

const theme = createMuiTheme({
  palette: {
    primary: { main: blue[800] },
    secondary: { main: red.A400 },
  },
  typography: { useNextVariants: true },
})

const Palette: React.SFC<{}> = (props: any) => {
  return <MuiThemeProvider theme={theme}>{props.children}</MuiThemeProvider>
}

export default Palette
