import * as React from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'

import BurgerBuilder from './views/Builder/BurgerBuilder'
import OrdersOverview from './views/Orders/OrdersOverview'

class Routes extends React.Component {
  public render() {
    return (
      <Switch>
        <Route path="/burger" component={BurgerBuilder} />
        <Route path="/orders" component={OrdersOverview} />
        <Redirect from="*" to="burger" />
      </Switch>
    )
  }
}

export default Routes
