import { Database } from './Firebase'
import IGenericItem from '../models/GenericApiItem'
import IOrder from '../models/Order'

// A helper function to generate a unique/random identifier
const generateId = (): string => {
  return Math.floor(Math.random() * Math.pow(10, 17)).toString(36)
}
const userId = generateId()

// A helper function to convert the JSON object from firebase to arrays of objects.
const convertItems = <T extends IGenericItem>(response: {
  [key: string]: T
}): T[] => {
  const items: T[] = []
  for (const id in response) {
    if (!response.hasOwnProperty(id)) {
      continue
    }
    items.push(response[id])
  }
  return items
}

// This is a shadowed API, which helps with making the calls to firebase.
// By organising it this way, multiple resources can be added with ease.
const firebaseAPI = {
  /**
   * Function to delete an document in firebase.
   * @param {string} reference The uri to the document that needs to be deleted.
   * @returns {boolean} Returns true if document was successfully deleted.
   */
  delete: async (reference: string): Promise<boolean> => {
    const orderRef = Database.ref(reference)
    await orderRef.remove()
    return true
  },

  /**
   * Function to retrieve an document in firebase.
   * @param {string} reference The uri to the document that needs to be retrieved.
   * @returns {T} Returns the document that was retrieved from firebase.
   */
  get: async <T extends IGenericItem>(reference: string): Promise<T> => {
    const orderRef = Database.ref(reference)
    const snapshot = await orderRef.remove()
    return snapshot.val()
  },

  /**
   * Function to retrieve all documents in a resource from firebase.
   * @param {string} reference The uri to the document resource that needs to be retrieved.
   * @returns {T[]} Returns the documents that were retrieved from firebase.
   */
  list: async <T extends IGenericItem>(reference: string): Promise<T[]> => {
    const orderRef = Database.ref(reference)
    const snapshot = await orderRef.once('value')
    return convertItems<T>(snapshot.val())
  },

  /**
   * Function to save a document in firebase.
   * If the document on the referenced uri already exists, the document will be overwritten.
   * @param {string} reference The uri to the document that needs to be saved.
   * @param {T} goodBoi The document that needs to be saved in firebase.
   * @returns {boolean} Returns true if document was successfully saved.
   */
  save: async <T extends IGenericItem>(
    reference: string,
    goodBoi: T
  ): Promise<boolean> => {
    const orderRef = Database.ref(reference)
    await orderRef.set(goodBoi)
    return true
  },

  /**
   * Function to open a stream to a specific resource or document.
   * With an open stream, changes made inside the resource or to the document will be pushed in real time.
   * @param {string} reference The uri to the resource or document that needs to be tracked.
   * @param {(streamedItems: T[]) => void} callback A function that will be called once an update is available.
   * @returns {firebase.database.Reference} Returns the reference to the open stream. This way it can be closed again when the stream is not in use anymore.
   */
  stream: <T extends IGenericItem>(
    reference: string,
    callback: (streamedItems: T[]) => void
  ): firebase.database.Reference => {
    const orderRef = Database.ref(reference)
    orderRef.on('value', snapshot => {
      if (snapshot) {
        const streamedItems = convertItems<T>(snapshot.val())
        callback(streamedItems)
      }
    })
    return orderRef
  },
}

const API = {
  orders: {
    delete: async (order: IOrder): Promise<boolean> =>
      await firebaseAPI.delete(`orders/${order.id}`),

    get: async (id: string): Promise<IOrder> =>
      await firebaseAPI.get(`orders/${id}`),

    list: async (): Promise<IOrder[]> => await firebaseAPI.list('orders'),

    save: async (order: IOrder): Promise<boolean> => {
      if (!order.id) {
        order.id = generateId()
      }
      if (!order.userId) {
        order.userId = userId
      }
      return await firebaseAPI.save(`orders/${order.id}`, order)
    },

    stream: (
      callback: (orders: IOrder[]) => void
    ): firebase.database.Reference => firebaseAPI.stream('orders', callback),
  },
}

export default API
