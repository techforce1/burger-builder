import { Auth } from './Firebase'

/**
 * A function to create a new user.
 * @param {string} email The email address of the new user.
 * @param {string} password The password of the new user.
 * @returns {firebase.auth.UserCredential} The newly created user.
 */
export const createUser = async (
  email: string,
  password: string
): Promise<firebase.auth.UserCredential> =>
  await Auth.createUserWithEmailAndPassword(email, password)

/**
 * A function to sign in an user.
 * @param {string} email The email address of the user.
 * @param {string} password The password of the user.
 * @returns {firebase.auth.UserCredential} The user object of the signed in user.
 */
export const signIn = async (
  email: string,
  password: string
): Promise<firebase.auth.UserCredential> =>
  await Auth.signInWithEmailAndPassword(email, password)

/**
 * A function to sign out a user.
 */
export const signOut = async (): Promise<void> => await Auth.signOut()

/**
 * A function to reset the password of an user. The user will recieve an email that can be used to reset his password.
 * @param {string} email The email address of the user.
 */
export const resetPassword = async (email: string): Promise<void> =>
  Auth.sendPasswordResetEmail(email)

/**
 * A function to change the password of an user.
 * @param {string} password The new password of the user.
 */
export const changePassword = async (password: string): Promise<void> => {
  if (Auth.currentUser) {
    Auth.currentUser.updatePassword(password)
  }
}

// The currently signed in user. Can also return `null` if no user is signed in yet.
export let User = Auth.currentUser

/**
 * A function that can be subscribed to, to get updates every time the signed in state of a user changes.
 * @param {(user: firebase.User | null) => void} callback A function that will be called once an update is available.
 */
export const userStateChanged = (
  callback: (user: firebase.User | null) => void
) =>
  Auth.onAuthStateChanged(user => {
    User = user
    callback(user)
  })
