import * as firebase from 'firebase/app'
import 'firebase/database'
import 'firebase/auth'

const config = {
  apiKey: process.env.REACT_APP_FIREBASE_API_KEY,
  authDomain: 'burger-builder-751ce.firebaseapp.com',
  databaseURL: process.env.REACT_APP_FIREBASE_URL,
}
firebase.initializeApp(config)

export const Auth = firebase.auth()
export const Database = firebase.database()
