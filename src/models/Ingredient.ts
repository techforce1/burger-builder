import IGenericItem from './GenericApiItem'
import INGREDIENT_TYPE from './IngredientType'

export default interface IIngredient extends IGenericItem {
  ingredient: INGREDIENT_TYPE
  price: number
}
