enum INGREDIENT_TYPE {
  MEAT = 'meat',
  LETTUCE = 'lettuce',
}

export default INGREDIENT_TYPE
