import IGenericItem from './GenericApiItem'
import INGREDIENT_TYPE from './IngredientType'

export default interface IOrder extends IGenericItem {
  userId?: string
  ingredients: INGREDIENT_TYPE[]
  createdAt: number
}
