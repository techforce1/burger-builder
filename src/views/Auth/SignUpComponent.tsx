import * as React from 'react'
import {
  Card,
  CardContent,
  TextField,
  Typography,
  withStyles,
  Theme,
  WithStyles,
  Button,
} from '@material-ui/core'

import { createUser } from '../../http/Auth'
import { Redirect } from 'react-router'

const styles = (theme: Theme) => ({
  textField: {
    marginRight: theme.spacing.unit,
  },
})

interface ISignUpProps extends WithStyles<typeof styles> {}

interface ISignUpState {
  email: string
  password: string
  redirect: boolean
}

class SignUpComponent extends React.Component<ISignUpProps, ISignUpState> {
  public state: ISignUpState = {
    email: '',
    password: '',
    redirect: false,
  }

  public render() {
    if (this.state.redirect) {
      return <Redirect to="/" />
    }
    const { classes } = this.props
    return (
      <Card>
        <CardContent>
          <Typography variant="h5">Create an Account</Typography>
          <form noValidate={true} autoComplete="off">
            <TextField
              id="email"
              className={classes.textField}
              label="e-mail"
              type="email"
              value={this.state.email}
              onChange={this.onChangeEmail}
            />
            <TextField
              id="password"
              className={classes.textField}
              label="password"
              type="password"
              value={this.state.password}
              onChange={this.onChangePassword}
            />
            <Button variant="contained" onClick={this.onSubmit}>
              Submit
            </Button>
          </form>
        </CardContent>
      </Card>
    )
  }

  private onChangeEmail = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({ email: event.target.value })
  }

  private onChangePassword = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({ password: event.target.value })
  }

  private onSubmit = async () => {
    const { email, password } = this.state
    await createUser(email, password)
    this.setState({ redirect: true })
  }
}

export default withStyles(styles)(SignUpComponent)
