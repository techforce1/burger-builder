import * as React from 'react'
import { Card, CardContent, Typography } from '@material-ui/core'

import BurgerComponent from './BurgerComponent'
import INGREDIENT_TYPE from '../../models/IngredientType'
import IngredientPicker from './IngredientPicker'

interface IBurgerBuilderState {
  ingredient: INGREDIENT_TYPE | undefined
}

class BurgerBuilder extends React.Component<{}, IBurgerBuilderState> {
  public state: IBurgerBuilderState = {
    ingredient: undefined,
  }
  public render() {
    return (
      <Card>
        <CardContent>
          <Typography variant="h5">Build a Burger</Typography>
          <IngredientPicker onIngredientPicked={this.onIngredientPicked} />
          <BurgerComponent ingredient={this.state.ingredient} />
        </CardContent>
      </Card>
    )
  }

  private onIngredientPicked = (ingredient: INGREDIENT_TYPE) => {
    this.setState({ ingredient })
  }
}

export default BurgerBuilder
