import * as React from 'react'
import { Theme, withStyles, WithStyles, Typography } from '@material-ui/core'
import amber from '@material-ui/core/colors/amber'

import INGREDIENT_TYPE from '../../models/IngredientType'
import MeatComponent from './Ingredients/MeatComponent'
import LettuceComponent from './Ingredients/LettuceComponent'

const burgerWidth = 500
const burgerColor = amber[700]
const seedColor = amber[100]

const styles = (theme: Theme) => ({
  bottomBun: {
    backgroundColor: burgerColor,
    borderBottomLeftRadius: theme.spacing.unit * 2,
    borderBottomRightRadius: theme.spacing.unit * 2,
    borderTopLeftRadius: theme.spacing.unit / 2,
    borderTopRightRadius: theme.spacing.unit / 2,
    height: 50,
    width: '95%',
  },
  root: {
    alignItems: 'center',
    display: 'flex',
    'flex-direction': 'column',
    margin: `${theme.spacing.unit * 2}px auto`,
    maxWidth: burgerWidth,
    width: '90%',
  },
  seed: {
    backgroundColor: seedColor,
    borderRadius: 10,
    height: theme.spacing.unit * 2,
    position: 'absolute' as 'absolute',
    width: theme.spacing.unit * 2,

    '&:nth-child(1)': {
      left: '20%',
      top: -30,
    },

    '&:nth-child(2)': {
      left: '26%',
      top: -10,
    },

    '&:nth-child(3)': {
      left: '30%',
      top: -40,
    },
  },
  topBun: {
    backgroundColor: burgerColor,
    borderBottomLeftRadius: theme.spacing.unit / 2,
    borderBottomRightRadius: theme.spacing.unit / 2,
    height: 20,
    marginBottom: theme.spacing.unit,
    marginTop: 50,
    position: 'relative' as 'relative',
    width: '95%',

    '&:before': {
      backgroundColor: burgerColor,
      borderBottomLeftRadius: 0,
      borderBottomRightRadius: 0,
      borderRadius: '50% / 100%',
      content: '""',
      height: 51,
      position: 'absolute' as 'absolute',
      top: -50,
      width: '100%',
    },
  },
})

interface IBurgerProps extends WithStyles<typeof styles> {
  ingredient?: INGREDIENT_TYPE
}

const BurgerComponent: React.SFC<IBurgerProps> = props => {
  const { classes } = props
  let ingredient: React.ReactNode
  switch (props.ingredient) {
    case INGREDIENT_TYPE.MEAT:
      ingredient = <MeatComponent />
      break
    case INGREDIENT_TYPE.LETTUCE:
      ingredient = <LettuceComponent />
      break
    default:
      ingredient = <Typography variant="h6">A rather plain burger</Typography>
      break
  }
  return (
    <div className={classes.root}>
      <div className={classes.topBun}>
        <div className={classes.seed} />
        <div className={classes.seed} />
        <div className={classes.seed} />
      </div>
      {ingredient}
      <div className={classes.bottomBun} />
    </div>
  )
}

export default withStyles(styles)(BurgerComponent)
