import * as React from 'react'
import {
  Button,
  withStyles,
  Theme,
  WithStyles,
  Typography,
} from '@material-ui/core'

import INGREDIENT_TYPE from '../../models/IngredientType'

const styles = (theme: Theme) => ({
  button: {
    marginRight: theme.spacing.unit * 2,
  },
  root: {
    marginBottom: theme.spacing.unit * 2,
    marginTop: theme.spacing.unit * 2,
  },
})

interface IIngredientPickerProps extends WithStyles<typeof styles> {
  onIngredientPicked: (ingredient: INGREDIENT_TYPE) => void
}

const IngredientPicker: React.SFC<IIngredientPickerProps> = props => {
  const { classes } = props
  return (
    <div className={classes.root}>
      <Typography variant="body1">Pick your topping:</Typography>
      <Button
        variant="contained"
        className={classes.button}
        onClick={() => props.onIngredientPicked(INGREDIENT_TYPE.MEAT)}
      >
        Meat
      </Button>
      <Button
        variant="contained"
        className={classes.button}
        onClick={() => props.onIngredientPicked(INGREDIENT_TYPE.LETTUCE)}
      >
        Lettuce
      </Button>
    </div>
  )
}

export default withStyles(styles)(IngredientPicker)
