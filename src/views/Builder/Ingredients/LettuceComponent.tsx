import * as React from 'react'
import { withStyles, WithStyles, Theme } from '@material-ui/core'
import green from '@material-ui/core/colors/green'

const styles = (theme: Theme) => ({
  root: {
    backgroundColor: green[700],
    borderRadius: theme.spacing.unit,
    height: theme.spacing.unit * 3,
    marginBottom: theme.spacing.unit,
    width: '100%',
  },
})

interface ILettuceProps extends WithStyles<typeof styles> {}

const LettuceComponent: React.SFC<ILettuceProps> = props => {
  return <div className={props.classes.root} />
}

export default withStyles(styles)(LettuceComponent)
