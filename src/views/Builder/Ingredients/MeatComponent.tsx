import * as React from 'react'
import { withStyles, WithStyles, Theme } from '@material-ui/core'
import red from '@material-ui/core/colors/red'

const styles = (theme: Theme) => ({
  root: {
    backgroundColor: red[700],
    height: theme.spacing.unit * 2,
    marginBottom: theme.spacing.unit,
    width: '95%',
  },
})

interface IMeatProps extends WithStyles<typeof styles> {}

const MeatComponent: React.SFC<IMeatProps> = props => {
  return <div className={props.classes.root} />
}

export default withStyles(styles)(MeatComponent)
