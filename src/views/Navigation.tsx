import * as React from 'react'
import {
  AppBar,
  CssBaseline,
  Drawer,
  IconButton,
  Theme,
  Toolbar,
  Typography,
  withStyles,
  WithStyles,
  List,
  ListItemText,
  ListItemIcon,
  ListItem,
  Hidden,
} from '@material-ui/core/'
import { Fastfood, Kitchen, Menu } from '@material-ui/icons'

import Palette from '../Palette'
import { Link, withRouter, RouteComponentProps } from 'react-router-dom'

const drawerWidth = 240

const styles = (theme: Theme) => ({
  anchor: {
    textDecoration: 'none',
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
  },
  drawer: {
    flexShrink: 0,
    width: drawerWidth,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  root: {
    display: 'flex',
  },
  toolbar: theme.mixins.toolbar,
})

type INavigationProps = WithStyles<typeof styles> & RouteComponentProps

interface INavigationState {
  mobileOpen: boolean
}

class Navigation extends React.Component<INavigationProps, INavigationState> {
  public state: INavigationState = {
    mobileOpen: false,
  }

  public render() {
    const { classes } = this.props

    const drawerContent = (
      <List>
        <Link to="/burger" className={classes.anchor}>
          <ListItem
            button={true}
            selected={this.props.location.pathname.indexOf('/burger') === 0}
            onClick={this.hideDrawer}
          >
            <ListItemIcon>
              <Fastfood />
            </ListItemIcon>
            <ListItemText>Order Burger</ListItemText>
          </ListItem>
        </Link>
        <Link to="/orders" className={classes.anchor}>
          <ListItem
            button={true}
            selected={this.props.location.pathname.indexOf('/orders') === 0}
            onClick={this.hideDrawer}
          >
            <ListItemIcon>
              <Kitchen />
            </ListItemIcon>
            <ListItemText>View Orders</ListItemText>
          </ListItem>
        </Link>
      </List>
    )

    return (
      <Palette>
        <div className={classes.root}>
          <CssBaseline />
          <AppBar position="fixed" className={classes.appBar}>
            <Toolbar>
              <Hidden smUp={true} implementation="css">
                <IconButton
                  color="inherit"
                  aria-label="Open drawer"
                  onClick={this.handleDrawerToggle}
                >
                  <Menu />
                </IconButton>
              </Hidden>
              <Typography variant="h6" color="inherit" noWrap={true}>
                Burger Builder
              </Typography>
            </Toolbar>
          </AppBar>
          <nav>
            <Hidden smUp={true} implementation="css">
              <Drawer
                variant="temporary"
                anchor="left"
                open={this.state.mobileOpen}
                onClose={this.handleDrawerToggle}
                classes={{
                  paper: classes.drawerPaper,
                }}
                ModalProps={{
                  keepMounted: true,
                }}
              >
                {drawerContent}
              </Drawer>
            </Hidden>
            <Hidden xsDown={true} implementation="css">
              <Drawer
                className={classes.drawer}
                variant="permanent"
                classes={{
                  paper: classes.drawerPaper,
                }}
              >
                <div className={classes.toolbar} />
                {drawerContent}
              </Drawer>
            </Hidden>
          </nav>
          <main className={classes.content}>
            <div className={classes.toolbar} />
            {this.props.children}
          </main>
        </div>
      </Palette>
    )
  }

  private handleDrawerToggle = () => {
    this.setState(state => ({ mobileOpen: !state.mobileOpen }))
  }

  private hideDrawer = () => {
    this.setState({ mobileOpen: false })
  }
}

export default withStyles(styles)(withRouter(Navigation))
