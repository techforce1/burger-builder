import * as React from 'react'
import { Card, CardContent, Typography } from '@material-ui/core'

class OrdersOverview extends React.Component {
  public render() {
    return (
      <Card>
        <CardContent>
          <Typography variant="h5">Placed Orders</Typography>
        </CardContent>
      </Card>
    )
  }
}

export default OrdersOverview
